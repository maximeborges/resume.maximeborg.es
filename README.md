# resume.maximeborg.es

Maxime Borges online resume

## Dependencies

    npm install pug-cli -g

## Build

    # Building the HTML file
    pug src/index.pug -o public
    # Copy all the static files
    cp -r src/favicon.ico src/static public/
    # Render the PDF from Chromium in headless mode
    chromium --headless --no-sandbox --disable-gpu --print-to-pdf=public/resume_maxime_borges.pdf public/index.html

Then the `public/` folder can be deployed.